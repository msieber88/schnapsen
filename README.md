# Schnapsen 
![build status](https://gitlab.com/msieber88/schnapsen/badges/master/build.svg)
![test covergage](https://gitlab.com/msieber88/schnapsen/badges/master/coverage.svg)

This project implements a counter for the Austrian card game "Schnapsen". The UI is implemented in JavaFX and is optimized to run on a Raspberry Pi 2 with a 3.2" display.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

The application is entirely written in Java therefore the installation of Java 1.8 is needed in order start development.

### Installing

Clone the repository and start a build with "maven package"

## Deployment

Add additional notes about how to deploy this on a live system

## Built With

* [Maven](https://maven.apache.org/) - Dependency Management

## Contributing

Please read [CONTRIBUTING.md](CONTRIBUTING.md) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://gitlab.com/msieber88/schnapsen/tags). 

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
