package at.msi.schnapsen.counter.impl;

import at.msi.schnapsen.api.beans.Game;
import at.msi.schnapsen.api.beans.Player;

import java.util.ArrayList;
import java.util.List;

/**
 * Counter implementation for 4 players
 * Created by Michael Sieber on 01.03.2017.
 */
public class Counter4Players extends AbstractCounter {

    /**
     * Points needed to win the game
     */
    private static final int WIN_POINTS = 24;

    public Counter4Players(Game game) {
        super(game, WIN_POINTS);
    }

    @Override
    protected List<Player> getPlayerOrder() {
        List<Player> playerOrder = new ArrayList<>();

        Player player1 = getGame().getTeams().get(0).getPlayers().get(0);
        Player player2 = getGame().getTeams().get(0).getPlayers().get(1);
        Player player3 = getGame().getTeams().get(1).getPlayers().get(0);
        Player player4 = getGame().getTeams().get(1).getPlayers().get(1);

        if (player1.isDealer()) {
            playerOrder.add(player1);
            playerOrder.add(player3);
            playerOrder.add(player2);
            playerOrder.add(player4);
        } else if (player2.isDealer()) {
            playerOrder.add(player2);
            playerOrder.add(player4);
            playerOrder.add(player1);
            playerOrder.add(player3);
        } else if (player3.isDealer()) {
            playerOrder.add(player3);
            playerOrder.add(player2);
            playerOrder.add(player4);
            playerOrder.add(player1);
        } else {
            playerOrder.add(player4);
            playerOrder.add(player1);
            playerOrder.add(player3);
            playerOrder.add(player2);
        }

        return playerOrder;
    }
}
