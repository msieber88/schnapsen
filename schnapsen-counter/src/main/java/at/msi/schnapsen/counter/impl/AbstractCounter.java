package at.msi.schnapsen.counter.impl;

import at.msi.schnapsen.api.beans.Game;
import at.msi.schnapsen.api.beans.Player;
import at.msi.schnapsen.api.beans.Team;
import at.msi.schnapsen.counter.Counter;
import at.msi.schnapsen.counter.exceptions.CounterException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Super class for all counter implementations
 * Created by Michael on 04.03.2017.
 */
public abstract class AbstractCounter implements Counter {
    private Game game;
    private int winPoints;
    private List<Player> playerOrder;

    /**
     * Constructor for all counter implementations
     *
     * @param game      The game which is currently played
     * @param winPoints The points needed to win a game
     */
    protected AbstractCounter(Game game, int winPoints) {
        this.game = game;
        this.winPoints = winPoints;
        playerOrder= getPlayerOrder();
    }

    /**
     * Get the game
     *
     * @return The game
     */
    protected Game getGame() {
        return game;
    }

    /**
     * Check if the given team has won the game
     *
     * @param team The team for which it should be checked if it won
     * @return True if the team has won, false otherwise
     */
    protected boolean hasWon(Team team) {
        return team.getPoints() >= winPoints;
    }

    /**
     * This will reset the counters of all teams to 0
     */
    protected void resetCounter() {
        for (Team team :
                game.getTeams()) {
            team.setPoints(0);
        }
    }

    /**
     * Get the team by id
     *
     * @param id The id of the team
     * @return The team with the given id
     * @throws CounterException Thrown if no team with the given id exists
     */
    protected Team getTeamById(int id) throws CounterException {
        for (Team team :
                game.getTeams()) {
            if (team.getId() == id) {
                return team;
            }
        }

        throw new CounterException("Team id " + id + " does not exist");
    }

    /**
     * This will return the team with the lowest points.
     * If multiple teams have the same lowest points, all of them will be returned
     *
     * @return The list of teams with the lowest points
     */
    protected List<Team> getTeamWithLowestPoints() {
        List<Team> teams = new ArrayList<>(game.getTeams());
        List<Team> teamsWithLowestPoints = new ArrayList<>();
        Collections.sort(teams, Comparator.comparingInt(Team::getPoints));
        int lowestPoints = teams.get(0).getPoints();

        for (Team team :
                teams) {
            if (team.getPoints() == lowestPoints) {
                teamsWithLowestPoints.add(team);
            }
        }

        return teamsWithLowestPoints;
    }

    public Player getDealer() throws CounterException {
        for (Team team :
                game.getTeams()) {
            for (Player player :
                    team.getPlayers()) {
                if (player.isDealer()) {
                    return player;
                }
            }
        }

        throw new CounterException("Unable to determine current dealer");
    }

    public void addPoints(int teamId, int points) throws CounterException {
        Team team = getTeamById(teamId);
        team.setPoints(team.getPoints() + points);

        if (hasWon(team)) {
            setResult(team);
            resetCounter();
        }

        changeDealer();
    }

    /**
     * Sets the result to the team with the lowest points
     *
     * @param winner The winner of the game
     */
    protected void setResult(Team winner) {
        for (Team looser :
                getTeamWithLowestPoints()) {
            if (looser.getPoints() == 0) {
                looser.setSchneider(looser.getSchneider() + 1);
            } else {
                looser.setBummerl(looser.getBummerl() + 1);
            }
        }

        // TODO retourschneider???
    }

    /**
     * Change the dealer to the next dealer in the row
     *
     * @throws CounterException Thrown if the current dealer couldn't be found
     */
    protected void changeDealer() throws CounterException {
        Player currDealer = getDealer();

        for (int i = 0; i < playerOrder.size(); i++) {
            if (currDealer.equals(playerOrder.get(i))) {
                currDealer.setDealer(false);
                if (i + 1 == playerOrder.size()) {
                    playerOrder.get(0).setDealer(true);
                } else {
                    playerOrder.get(i + 1).setDealer(true);
                }

                return;
            }
        }
    }

    /**
     * Get the playing order of all players. This will be used to calculate the dealer
     * @return The list of players ordered by the dealing order
     */
    protected abstract List<Player> getPlayerOrder();

    public void undo() {

    }

    public void redo() {

    }
}
