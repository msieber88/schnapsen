package at.msi.schnapsen.counter;

import at.msi.schnapsen.api.beans.Player;
import at.msi.schnapsen.counter.exceptions.CounterException;

/**
 * Common interface of the Schnpasen counter
 * Created by Michael Sieber on 01.03.2017.
 */
public interface Counter {

    /**
     * Add the given points to the team with the given id
     *
     * @param teamId The id of the team to which the points should be added
     * @param points The points to add to the given team
     * @throws CounterException Thrown on errors during counting
     */
    void addPoints(int teamId, int points) throws CounterException;

    /**
     * Get the current dealer
     * @return The player which is currently the dealer
     * @throws CounterException Thrown if the current dealer couldn't be found
     */
    Player getDealer() throws CounterException;

    /**
     * Undo the last action
     */
    void undo();

    /**
     * Redo the last action
     */
    void redo();
}
