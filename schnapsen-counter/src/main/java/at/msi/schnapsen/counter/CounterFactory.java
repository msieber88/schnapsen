package at.msi.schnapsen.counter;

import at.msi.schnapsen.api.beans.Game;
import at.msi.schnapsen.api.beans.Team;
import at.msi.schnapsen.counter.exceptions.CounterInitializationException;
import at.msi.schnapsen.counter.impl.Counter2Players;
import at.msi.schnapsen.counter.impl.Counter3Players;
import at.msi.schnapsen.counter.impl.Counter4Players;

/**
 * Factory class which creates the counter object for the game depending on the number of players and teams.
 * Created by Michael Sieber on 01.03.2017.
 */
public final class CounterFactory {

    /**
     * Private constructor for utility class with only static methods
     */
    private CounterFactory() {
    }

    /**
     * Get the counter for the given game. This will validate the game object before initializing the counter
     *
     * @param game The game for which a counter should be initialized
     * @return The counter needed for the given game
     * @throws CounterInitializationException Thrown on errors during counter initialization, e.g. if the wrong number
     *                                        of players is specified
     */
    public static Counter getCounter(Game game) throws CounterInitializationException {
        validate(game);
        int players = getPlayers(game);

        switch (players) {
            case 2:
                return new Counter2Players(game);
            case 3:
                return new Counter3Players(game);
            case 4:
                return new Counter4Players(game);
            default:
                throw new CounterInitializationException("Unsupported number of players: " + players);
        }
    }

    /**
     * Calcuate the number of players for the given game
     *
     * @param game The game for which the number of players should be calculated
     * @return The number of players within the given game
     */
    protected static int getPlayers(Game game) {
        if (game == null || game.getTeams() == null) {
            return 0;
        }

        int players = 0;
        for (Team team :
                game.getTeams()) {
            if (team.getPlayers() != null) {
                players += team.getPlayers().size();
            }
        }

        return players;
    }

    /**
     * Validate the game if there is enough information provided to initialize a counter
     *
     * @param game The game which should be validated
     * @throws CounterInitializationException Thrown on any errors during validation
     */
    protected static void validate(Game game) throws CounterInitializationException {
        if (game == null) {
            throw new CounterInitializationException("Game must not be null");
        }

        if (game.getTeams() == null || game.getTeams().isEmpty()) {
            throw new CounterInitializationException("A game must have at least one team");
        }

        if (game.getTeams().size() < 2) {
            throw new CounterInitializationException("A game must have at least 2 teams");
        }

        int player = -1;
        for (Team team : game.getTeams()) {
            if (team.getPlayers() == null || team.getPlayers().isEmpty()) {
                throw new CounterInitializationException("A team must have at least one player");
            }

            if (player == -1) {
                player = team.getPlayers().size();
            } else if (team.getPlayers().size() != player) {
                throw new CounterInitializationException("All teams must have the same amount of players");
            }
        }
    }
}
