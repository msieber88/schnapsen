package at.msi.schnapsen.counter.exceptions;

/**
 * Exception for any issues which happen during counting
 * Created by Michael on 01.03.2017.
 */
public class CounterException extends Exception {

    /**
     * Create a new {@link CounterException} with an error message
     *
     * @param message The error message
     */
    public CounterException(String message) {
        super(message);
    }

    /**
     * Create a new {@link CounterException} with an error message and the cause of the error
     *
     * @param message The error message
     * @param cause   The cause of the error
     */
    public CounterException(String message, Throwable cause) {
        super(message, cause);
    }
}
