package at.msi.schnapsen.counter.impl;

import at.msi.schnapsen.api.beans.Game;
import at.msi.schnapsen.api.beans.Player;
import at.msi.schnapsen.api.beans.Team;
import at.msi.schnapsen.counter.Counter;
import at.msi.schnapsen.counter.exceptions.CounterException;

import java.util.ArrayList;
import java.util.List;

/**
 * Counter implementation for 2 players
 * Created by Michael Sieber on 01.03.2017.
 */
public class Counter2Players extends AbstractCounter {

    /**
     * Points needed to win the game
     */
    private static final int WIN_POINTS = 7;

    /**
     * Create a new {@link Counter2Players}
     *
     * @param game The game for which the counter should be executed
     */
    public Counter2Players(Game game) {
        super(game, WIN_POINTS);
    }

    @Override
    protected List<Player> getPlayerOrder() {
        List<Player> playerOrder = new ArrayList<>();

        Player player1 = getGame().getTeams().get(0).getPlayers().get(0);
        Player player2 = getGame().getTeams().get(1).getPlayers().get(0);

        if (player1.isDealer()) {
            playerOrder.add(player1);
            playerOrder.add(player2);
        } else {
            playerOrder.add(player2);
            playerOrder.add(player1);
        }

        return playerOrder;
    }
}
