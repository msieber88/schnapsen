package at.msi.schnapsen.counter.exceptions;

/**
 * Exception for any issues which happen during initialization of the counter
 * Created by Michael on 01.03.2017.
 */
public class CounterInitializationException extends Exception {

    /**
     * Create a new {@link CounterInitializationException} with an error message
     *
     * @param message The error message
     */
    public CounterInitializationException(String message) {
        super(message);
    }

    /**
     * Create a new {@link CounterInitializationException} with an error message and the cause of the error
     *
     * @param message The error message
     * @param cause   The cause of the error
     */
    public CounterInitializationException(String message, Throwable cause) {
        super(message, cause);
    }
}
