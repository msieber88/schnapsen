package at.msi.schnapsen.counter.impl;

import at.msi.schnapsen.api.beans.Game;
import at.msi.schnapsen.api.beans.Player;
import at.msi.schnapsen.api.beans.Team;
import at.msi.schnapsen.counter.CounterFactory;
import at.msi.schnapsen.counter.exceptions.CounterException;
import at.msi.schnapsen.counter.exceptions.CounterInitializationException;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Tests for {@link Counter2Players}
 * Created by Michael Sieber on 18.03.2017.
 */
public class Counter2PlayerTest {

    private Game game;
    private Counter2Players counter;

    @Before
    public void prepare2PlayerGame() throws CounterInitializationException {
        game = new Game();
        game.setTeams(new ArrayList<>());

        Team team1 = new Team();
        team1.setId(1);
        team1.setPlayers(new ArrayList<>());
        game.getTeams().add(team1);

        Player player1 = new Player();
        player1.setId(1);
        player1.setName("Michael");
        player1.setDealer(true);
        team1.getPlayers().add(player1);

        Team team2 = new Team();
        team2.setId(2);
        team2.setPlayers(new ArrayList<>());
        game.getTeams().add(team2);

        Player player2 = new Player();
        player2.setId(2);
        player2.setName("Kurt");
        team2.getPlayers().add(player2);

        counter = (Counter2Players) CounterFactory.getCounter(game);
    }

    /**
     * Get the team by id
     *
     * @param teamId The id of the team
     * @return The team with the given id
     */
    private Team getTeamById(int teamId) {
        return game.getTeams().stream().filter(t -> t.getId() == teamId).findFirst().get();
    }

    /**
     * Test if the counter gets reset correctly for all teams
     */
    @Test
    public void testResetCounter() {
        counter.resetCounter();
        for (Team team :
                game.getTeams()) {
            assertEquals(0, team.getPoints());
        }
    }

    /**
     * Test the loading of the team with the lowest points
     */
    @Test
    public void testGetTeamWithLowestPoints(){
        getTeamById(1).setPoints(7);
        List<Team> loosers = counter.getTeamWithLowestPoints();

        assertEquals(1, loosers.size());
        assertEquals(2, loosers.get(0).getId());
    }

    /**
     * Test changing the dealer
     */
    @Test
    public void testChangeDealer() throws CounterException {
        assertTrue(game.getTeams().get(0).getPlayers().get(0).isDealer());
        counter.changeDealer();
        assertTrue(game.getTeams().get(1).getPlayers().get(0).isDealer());
        counter.changeDealer();
        assertTrue(game.getTeams().get(0).getPlayers().get(0).isDealer());
    }

    /**
     * Test getting the current dealer
     * @throws CounterException
     */
    @Test
    public void testGetDealer() throws CounterException {
        assertEquals(1, counter.getDealer().getId());
    }

    /**
     * Test getting the dealer, if no dealer is set
     */
    @Test(expected = CounterException.class)
    public void testGetDealerError() throws CounterInitializationException, CounterException {
        Game game = new Game();
        game.setTeams(new ArrayList<>());

        Team team1 = new Team();
        team1.setId(1);
        team1.setPlayers(new ArrayList<>());
        game.getTeams().add(team1);

        Player player1 = new Player();
        player1.setId(1);
        player1.setName("Michael");
        player1.setDealer(false);
        team1.getPlayers().add(player1);

        Team team2 = new Team();
        team2.setId(2);
        team2.setPlayers(new ArrayList<>());
        game.getTeams().add(team2);

        Player player2 = new Player();
        player2.setId(2);
        player2.setName("Kurt");
        player2.setDealer(false);
        team2.getPlayers().add(player2);

        Counter2Players counter = (Counter2Players) CounterFactory.getCounter(game);
        counter.getDealer();
    }
}
