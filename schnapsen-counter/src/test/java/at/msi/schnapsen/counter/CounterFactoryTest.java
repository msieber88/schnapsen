package at.msi.schnapsen.counter;

import at.msi.schnapsen.api.beans.Game;
import at.msi.schnapsen.api.beans.Player;
import at.msi.schnapsen.api.beans.Team;
import at.msi.schnapsen.counter.exceptions.CounterInitializationException;
import at.msi.schnapsen.counter.impl.Counter2Players;
import at.msi.schnapsen.counter.impl.Counter3Players;
import at.msi.schnapsen.counter.impl.Counter4Players;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Collections;

/**
 * Tests for {@link CounterFactory}
 * Created by Michael Sieber on 01.03.2017.
 */
public class CounterFactoryTest {

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    /**
     * Test {@link CounterFactory#getPlayers(Game)} with the game set to null
     */
    @Test
    public void testGetPlayersWithNullGame() {
        assertEquals(0, CounterFactory.getPlayers(null));
    }

    /**
     * Test {@link CounterFactory#getPlayers(Game)} with the list of teams set to null
     */
    @Test
    public void testGetPlayersWithNullTeams() {
        Game game = new Game();
        game.setTeams(null);

        assertEquals(0, CounterFactory.getPlayers(game));
    }

    /**
     * Test {@link CounterFactory#getPlayers(Game)} with the list an empty list of teams
     */
    @Test
    public void testGetPlayersWithEmptyTeams() {
        Game game = new Game();
        game.setTeams(Collections.<Team>emptyList());

        assertEquals(0, CounterFactory.getPlayers(game));
    }

    /**
     * Test {@link CounterFactory#getPlayers(Game)} with a null list of players within the team
     */
    @Test
    public void testGetPlayersWithNullPlayers() {
        Game game = new Game();
        Team team = new Team();
        team.setPlayers(null);
        game.setTeams(Arrays.asList(team));

        assertEquals(0, CounterFactory.getPlayers(game));
    }

    /**
     * Test {@link CounterFactory#getPlayers(Game)} with an empty list of players within the team
     */
    @Test
    public void testGetPlayersWithEmptyPlayers() {
        Game game = new Game();
        Team team = new Team();
        team.setPlayers(Collections.<Player>emptyList());
        game.setTeams(Arrays.asList(team));

        assertEquals(0, CounterFactory.getPlayers(game));
    }

    /**
     * Test {@link CounterFactory#getPlayers(Game)} with 2 players
     */
    @Test
    public void testGetPlayersWith2Players() {
        Game game = new Game();

        Team team1 = new Team();
        Player player1 = new Player();
        team1.setPlayers(Arrays.asList(player1));

        Team team2 = new Team();
        Player player2 = new Player();
        team2.setPlayers(Arrays.asList(player2));

        game.setTeams(Arrays.asList(team1, team2));

        assertEquals(2, CounterFactory.getPlayers(game));
    }

    /**
     * Test {@link CounterFactory#getPlayers(Game)} with 3 players
     */
    @Test
    public void testGetPlayersWith3Players() {
        Game game = new Game();

        Team team1 = new Team();
        Player player1 = new Player();
        team1.setPlayers(Arrays.asList(player1));

        Team team2 = new Team();
        Player player2 = new Player();
        team2.setPlayers(Arrays.asList(player2));

        Team team3 = new Team();
        Player player3 = new Player();
        team3.setPlayers(Arrays.asList(player3));

        game.setTeams(Arrays.asList(team1, team2, team3));

        assertEquals(3, CounterFactory.getPlayers(game));
    }

    /**
     * Test {@link CounterFactory#getPlayers(Game)} with 4 players
     */
    @Test
    public void testGetPlayersWith4Players() {
        Game game = new Game();

        Team team1 = new Team();
        Player player1 = new Player();
        Player player2 = new Player();
        team1.setPlayers(Arrays.asList(player1, player2));

        Team team2 = new Team();
        Player player3 = new Player();
        Player player4 = new Player();
        team2.setPlayers(Arrays.asList(player3, player4));

        game.setTeams(Arrays.asList(team1, team2));

        assertEquals(4, CounterFactory.getPlayers(game));
    }

    /**
     * Test {@link CounterFactory#getCounter(Game)} with 1 player
     */
    @Test
    public void testGetCounterWith1Player() throws CounterInitializationException {
        expectedException.expect(CounterInitializationException.class);
        expectedException.expectMessage("A game must have at least 2 teams");

        Game game = new Game();
        Team team1 = new Team();
        Player player1 = new Player();
        team1.setPlayers(Arrays.asList(player1));
        game.setTeams(Arrays.asList(team1));

        CounterFactory.getCounter(game);
    }

    /**
     * Test {@link CounterFactory#getCounter(Game)} with 2 players
     */
    @Test
    public void testGetCounterWith2Players() throws CounterInitializationException {
        Game game = new Game();

        Team team1 = new Team();
        Player player1 = new Player();
        team1.setPlayers(Arrays.asList(player1));

        Team team2 = new Team();
        Player player2 = new Player();
        team2.setPlayers(Arrays.asList(player2));

        game.setTeams(Arrays.asList(team1, team2));

        assertEquals(Counter2Players.class, CounterFactory.getCounter(game).getClass());
    }

    /**
     * Test {@link CounterFactory#getCounter(Game)} with 3 players
     */
    @Test
    public void testGetCounterWith3Players() throws CounterInitializationException {
        Game game = new Game();

        Team team1 = new Team();
        Player player1 = new Player();
        team1.setPlayers(Arrays.asList(player1));

        Team team2 = new Team();
        Player player2 = new Player();
        team2.setPlayers(Arrays.asList(player2));

        Team team3 = new Team();
        Player player3 = new Player();
        team3.setPlayers(Arrays.asList(player3));

        game.setTeams(Arrays.asList(team1, team2, team3));

        assertEquals(Counter3Players.class, CounterFactory.getCounter(game).getClass());
    }

    /**
     * Test {@link CounterFactory#getCounter(Game)} with 4 players
     */
    @Test
    public void testGetCounterWith4Players() throws CounterInitializationException {
        Game game = new Game();

        Team team1 = new Team();
        Player player1 = new Player();
        Player player2 = new Player();
        team1.setPlayers(Arrays.asList(player1, player2));

        Team team2 = new Team();
        Player player3 = new Player();
        Player player4 = new Player();
        team2.setPlayers(Arrays.asList(player3, player4));

        game.setTeams(Arrays.asList(team1, team2));

        assertEquals(Counter4Players.class, CounterFactory.getCounter(game).getClass());
    }

    /**
     * Test {@link CounterFactory#validate(Game)} with null game
     */
    @Test
    public void testValidateWithNullGame() throws CounterInitializationException {
        expectedException.expect(CounterInitializationException.class);
        expectedException.expectMessage("Game must not be null");

        CounterFactory.validate(null);
    }

    /***
     * Test {@link CounterFactory#validate(Game)} with null team list
     */
    @Test
    public void testValidateWithNullTeam() throws CounterInitializationException {
        expectedException.expect(CounterInitializationException.class);
        expectedException.expectMessage("A game must have at least one team");

        Game game = new Game();
        game.setTeams(null);
        CounterFactory.validate(game);
    }

    /**
     * Test {@link CounterFactory#validate(Game)} with empty team list
     */
    @Test
    public void testValidateWithEmptyTeam() throws CounterInitializationException {
        expectedException.expect(CounterInitializationException.class);
        expectedException.expectMessage("A game must have at least one team");

        Game game = new Game();
        game.setTeams(Collections.<Team>emptyList());
        CounterFactory.validate(game);
    }

    /***
     * Test {@link CounterFactory#validate(Game)} with null player list within the team
     */
    @Test
    public void testValidateWithNullPlayer() throws CounterInitializationException {
        expectedException.expect(CounterInitializationException.class);
        expectedException.expectMessage("A game must have at least 2 teams");

        Game game = new Game();
        Team team = new Team();
        team.setPlayers(null);
        game.setTeams(Arrays.asList(team));
        CounterFactory.validate(game);
    }

    /**
     * Test {@link CounterFactory#validate(Game)} with an empty player list within the team
     */
    @Test
    public void testValidateWithEmptyPlayer() throws CounterInitializationException {
        expectedException.expect(CounterInitializationException.class);
        expectedException.expectMessage("A game must have at least 2 teams");

        Game game = new Game();
        Team team = new Team();
        team.setPlayers(Collections.<Player>emptyList());
        game.setTeams(Arrays.asList(team));
        CounterFactory.validate(game);
    }

    /**
     * Test {@link CounterFactory#validate(Game)} with a different amount of players per team
     */
    @Test
    public void testValidateDifferentNumberOfPlayersInTeams()throws CounterInitializationException {
        expectedException.expect(CounterInitializationException.class);
        expectedException.expectMessage("All teams must have the same amount of players");

        Game game = new Game();

        Team team1 = new Team();
        Player player1 = new Player();
        Player player2 = new Player();
        team1.setPlayers(Arrays.asList(player1, player2));

        Team team2 = new Team();
        Player player3 = new Player();
        team2.setPlayers(Arrays.asList(player3));

        game.setTeams(Arrays.asList(team1, team2));

        CounterFactory.validate(game);
    }
}
