package at.msi.schnapsen.counter.impl;

import at.msi.schnapsen.api.beans.Game;
import at.msi.schnapsen.api.beans.Player;
import at.msi.schnapsen.api.beans.Team;
import at.msi.schnapsen.counter.Counter;
import at.msi.schnapsen.counter.CounterFactory;
import at.msi.schnapsen.counter.exceptions.CounterException;
import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import static org.junit.Assert.*;

import java.util.ArrayList;

/**
 * Behavior tests for 2 players
 * Created by Michael Sieber on 18.03.2017.
 */
public class CounterBehaviorIT {

    private Game game = new Game();
    private Counter counter;

    /**
     * Get the team by id
     *
     * @param teamId The id of the team
     * @return The team with the given id
     */
    private Team getTeamById(int teamId) {
        return game.getTeams().stream().filter(t -> t.getId() == teamId).findFirst().get();
    }

    /**
     * Create the amount of teams with the defined amount of players
     *
     * @param teams   Number of teams
     * @param players Number of players in each team
     */
    @Given("^(\\d+) teams with (\\d+) players each$")
    public void teamsWithPlayersEach(int teams, int players) throws Exception {
        game.setTeams(new ArrayList<>());

        for (int i = 1; i <= teams; i++) {
            Team team = new Team();
            team.setId(i);
            team.setPlayers(new ArrayList<>());
            game.getTeams().add(team);

            for (int j = 1; j <= players; j++) {
                Player player = new Player();
                player.setId(j);
                team.getPlayers().add(player);
            }
        }

        counter = CounterFactory.getCounter(game);
    }

    /**
     * Set the name of the players in the team
     *
     * @param teamId     The team for which the name of the player should be set
     * @param playerId   The id of the player for which the name should be set
     * @param playerName The name of the player to set
     */
    @And("^Team (\\d+) with player (\\d+) ([a-zA-Z]+)")
    public void teamWithPlayerName(int teamId, int playerId, String playerName) {
        for (Player player :
                getTeamById(teamId).getPlayers()) {
            if(player.getId() == playerId){
                player.setName(playerName);
            }
        }
    }

    /**
     * Set the points of the given team to the given initial value
     *
     * @param teamId Team number
     * @param points Points to initialize
     */
    @Given("^Team (\\d+) has (\\d+) points$")
    public void teamHasPoints(int teamId, int points) {
        getTeamById(teamId).setPoints(points);
    }

    /**
     * Add points to a team
     *
     * @param teamId The id of the team to which the points should be added
     * @param points The points to add
     */
    @When("^Team (\\d+) wins a game worth (\\d+) points$")
    public void teamWinsAGameWorthPoints(int teamId, int points) throws CounterException {
        counter.addPoints(teamId, points);
    }

    /**
     * Assert that the given team has the given points
     *
     * @param teamId The id of the team for which the points should be checked
     * @param points The points which the given team should have
     */
    @Then("^Team (\\d+) should have (\\d+) points$")
    public void teamShouldHavePoints(int teamId, int points) {
        assertEquals(points, getTeamById(teamId).getPoints());
    }

    /**
     * Assert that the given team has the given amount of bummerl
     *
     * @param teamId  The id of the team for which the bummerl should be checked
     * @param bummerl The number of bummerl the given team should have
     */
    @And("^Team (\\d+) should have (\\d+) bummerl$")
    public void teamShouldHaveBummerl(int teamId, int bummerl) {
        assertEquals(bummerl, getTeamById(teamId).getBummerl());
    }

    /**
     * Assert that the given team has the given amount of schneider
     *
     * @param teamId    The id of the team for which the schneider should be checked
     * @param schneider The number of schneider the given team should have
     */
    @And("^Team (\\d+) should have (\\d+) schneider$")
    public void teamShouldHaveSchneider(int teamId, int schneider) {
        assertEquals(schneider, getTeamById(teamId).getSchneider());
    }

    /**
     * Assert that the given team has the given amount of retourschneider
     *
     * @param teamId          The id of the team for which the retourschneider should be checked
     * @param retourschneider The number of retourschneider the given team should have
     */
    @And("^Team (\\d+) should have (\\d+) retourschneider$")
    public void teamShouldHaveRetourschneider(int teamId, int retourschneider) {
        assertEquals(retourschneider, getTeamById(teamId).getRetourSchneider());
    }

    /**
     * Check if the given player is currently the dealer
     *
     * @param playerName The name of the player which should be the dealer
     * @throws CounterException Thrown if the dealer couldn't be found
     */
    @Given("^([a-zA-Z]+) is the dealer$")
    public void playerIsTheDealer(String playerName) throws CounterException {
        assertNotNull(counter.getDealer());
        assertEquals(playerName, counter.getDealer().getName());
    }

    /**
     * Set the dealer flag for the given player
     *
     * @param playerName The name of the player which should be the initial dealer
     */
    @And("^([a-zA-Z]+) is the initial dealer$")
    public void michaelIsTheInitialDealer(String playerName) {
        for (Team team :
                game.getTeams()) {
            for (Player player :
                    team.getPlayers()) {
                if (playerName.equals(player.getName())) {
                    player.setDealer(true);
                }
            }
        }
    }
}
