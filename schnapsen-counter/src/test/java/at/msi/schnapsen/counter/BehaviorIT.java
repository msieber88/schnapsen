package at.msi.schnapsen.counter;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

/**
 * Runner for all cucumber tests
 * Created by Michael Sieber on 18.03.2017.
 */
@RunWith(Cucumber.class)
@CucumberOptions(
        format = { "pretty", "html:target/cucumber" },
        features = "classpath:cucumber/"
)
public class BehaviorIT {
}
