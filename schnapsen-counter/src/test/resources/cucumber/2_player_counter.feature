Feature: Counter tests for 2 players
  This feature implements test cases for 2 player games

  Background:
    Given 2 teams with 1 players each
    And Team 1 with player 1 Michael
    And  Team 2 with player 1 Thomas
    And Michael is the initial dealer

  Scenario: Add points to a team on a new game where both teams have 0 points
    Given Team 1 has 0 points
    And Team 2 has 0 points
    When Team 1 wins a game worth 6 points
    Then Team 1 should have 6 points
    And Team 2 should have 0 points

  Scenario: Add points to a team which already has won some games
    Given Team 1 has 2 points
    And Team 2 has 0 points
    When Team 1 wins a game worth 3 points
    Then Team 1 should have 5 points
    And Team 2 should have 0 points

  Scenario: Add points and win the game with bummerl as result
    Given Team 1 has 5 points
    And Team 2 has 4 points
    When Team 1 wins a game worth 2 points
    Then Team 1 should have 0 points
    And Team 2 should have 0 points
    And Team 1 should have 0 bummerl
    And Team 2 should have 1 bummerl

  Scenario: Add points and win the game with schneider as result
    Given Team 1 has 5 points
    And Team 2 has 0 points
    When Team 1 wins a game worth 3 points
    Then Team 1 should have 0 points
    And Team 2 should have 0 points
    And Team 1 should have 0 schneider
    And Team 2 should have 1 schneider

  Scenario: Add points and win the game with retourschneider as result
    Given Team 1 has 6 points
    And Team 2 has 0 points
    When Team 2 wins a game worth 7 points
    Then Team 1 should have 0 points
    And Team 2 should have 0 points
    And Team 1 should have 1 retourschneider
    And Team 2 should have 0 retourschneider

  Scenario: Simulate simple game
    Given Team 1 has 0 points
    And Team 2 has 0 points
    When Team 1 wins a game worth 2 points
    And Team 1 wins a game worth 3 points
    And Team 2 wins a game worth 1 points
    Then Team 1 should have 5 points
    And Team 2 should have 1 points

  Scenario: Change dealer after a game has been finished
    Given Michael is the dealer
    When Team 2 wins a game worth 1 points
    Then Thomas is the dealer