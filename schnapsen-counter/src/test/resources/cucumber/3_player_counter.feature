Feature: Counter tests for 3 players
  This feature implements test cases for 3 player games

  Background:
    Given 3 teams with 1 players each
    And Team 1 with player 1 Michael
    And  Team 2 with player 1 Thomas
    And Team 3 with player 1 Kurt
    And Michael is the initial dealer

  Scenario: Add points to a team on a new game where all teams have 0 points
    Given Team 1 has 0 points
    And Team 2 has 0 points
    And Team 3 has 0 points
    When Team 1 wins a game worth 6 points
    Then Team 1 should have 6 points
    And Team 2 should have 0 points
    And Team 3 should have 0 points

  Scenario: Add points to a team which already has won some games
    Given Team 1 has 12 points
    And Team 2 has 0 points
    And Team 3 has 5 points
    When Team 1 wins a game worth 10 points
    Then Team 1 should have 22 points
    And Team 2 should have 0 points
    And Team 3 should have 5 points

  Scenario: Add points and win the game with bummerl as result
    Given Team 1 has 22 points
    And Team 2 has 5 points
    And Team 3 has 2 points
    When Team 1 wins a game worth 5 points
    Then Team 1 should have 0 points
    And Team 2 should have 0 points
    And Team 3 should have 0 points
    And Team 1 should have 0 bummerl
    And Team 2 should have 0 bummerl
    And Team 3 should have 1 bummerl

  Scenario: Add points and win the game with double bummerl as result
    Given Team 1 has 22 points
    And Team 2 has 5 points
    And Team 3 has 5 points
    When Team 1 wins a game worth 5 points
    Then Team 1 should have 0 points
    And Team 2 should have 0 points
    And Team 3 should have 0 points
    And Team 1 should have 0 bummerl
    And Team 2 should have 1 bummerl
    And Team 3 should have 1 bummerl

  Scenario: Add points and win the game with schneider as result
    Given Team 1 has 16 points
    And Team 2 has 0 points
    And Team 3 has 3 points
    When Team 1 wins a game worth 10 points
    Then Team 1 should have 0 points
    And Team 2 should have 0 points
    And Team 3 should have 0 points
    And Team 1 should have 0 schneider
    And Team 2 should have 1 schneider
    And Team 3 should have 0 schneider

  Scenario: Add points and win the game with double schneider as result
    Given Team 1 has 16 points
    And Team 2 has 0 points
    And Team 3 has 0 points
    When Team 1 wins a game worth 10 points
    Then Team 1 should have 0 points
    And Team 2 should have 0 points
    And Team 3 should have 0 points
    And Team 1 should have 0 schneider
    And Team 2 should have 1 schneider
    And Team 3 should have 1 schneider

  Scenario: Add points and win the game with retourschneider as result
    Given Team 1 has 23 points
    And Team 2 has 0 points
    And Team 3 has 5 points
    When Team 2 wins a game worth 24 points
    Then Team 1 should have 0 points
    And Team 2 should have 0 points
    And Team 3 should have 0 points
    And Team 1 should have 1 retourschneider
    And Team 2 should have 0 retourschneider
    And Team 3 should have 0 retourschneider
    And Team 3 should have 0 bummerl

  Scenario: Add points and win the game with double retourschneider as result
    Given Team 1 has 23 points
    And Team 2 has 0 points
    And Team 3 has 23 points
    When Team 2 wins a game worth 24 points
    Then Team 1 should have 0 points
    And Team 2 should have 0 points
    And Team 3 should have 0 points
    And Team 1 should have 1 retourschneider
    And Team 2 should have 0 retourschneider
    And Team 3 should have 1 retourschneider

  Scenario: Simulate simple game
    Given Team 1 has 0 points
    And Team 2 has 0 points
    And Team 3 has 0 points
    When Team 1 wins a game worth 10 points
    And Team 1 wins a game worth 6 points
    And Team 2 wins a game worth 8 points
    And Team 3 wins a game worth 5 points
    Then Team 1 should have 16 points
    And Team 2 should have 8 points
    And Team 3 should have 5 points

  Scenario: Change dealer after a game has been finished
    Given Michael is the dealer
    When Team 3 wins a game worth 5 points
    Then Thomas is the dealer