package at.msi.schnapsen.api.beans;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Representation of game
 * Created by Michael Sieber on 22.02.2017.
 */
public final class Game implements Serializable {
    private Date start;
    private Date end;
    private List<Team> teams;
    private List<GamePart> gameParts;

    public Date getStart() {
        return start;
    }

    public void setStart(Date start) {
        this.start = start;
    }

    public Date getEnd() {
        return end;
    }

    public void setEnd(Date end) {
        this.end = end;
    }

    public List<Team> getTeams() {
        return teams;
    }

    public void setTeams(List<Team> teams) {
        this.teams = teams;
    }

    public List<GamePart> getGameParts() {
        return gameParts;
    }

    public void setGameParts(List<GamePart> gameParts) {
        this.gameParts = gameParts;
    }

    @Override
    public String toString() {
        return "Game{" +
                "start=" + start +
                ", end=" + end +
                ", teams=" + teams +
                ", gameParts=" + gameParts +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Game game = (Game) o;

        if (start != null ? !start.equals(game.start) : game.start != null) return false;
        if (end != null ? !end.equals(game.end) : game.end != null) return false;
        if (teams != null ? !teams.equals(game.teams) : game.teams != null) return false;
        return gameParts != null ? gameParts.equals(game.gameParts) : game.gameParts == null;
    }

    @Override
    public int hashCode() {
        int result = start != null ? start.hashCode() : 0;
        result = 31 * result + (end != null ? end.hashCode() : 0);
        result = 31 * result + (teams != null ? teams.hashCode() : 0);
        result = 31 * result + (gameParts != null ? gameParts.hashCode() : 0);
        return result;
    }
}
