package at.msi.schnapsen.api.beans;


import java.io.Serializable;
import java.util.List;

/**
 * Representation of a team within the game
 * Created by Michael Sieber on 18.02.2017.
 */
public final class Team implements Serializable {
    private static final String TEAM_NAME_SEPARATOR = " & ";
    private int id;
    private List<Player> players;
    private int points;
    private int bummerl;
    private int schneider;
    private int retourSchneider;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<Player> getPlayers() {
        return players;
    }

    public void setPlayers(List<Player> players) {
        this.players = players;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public int getBummerl() {
        return bummerl;
    }

    public void setBummerl(int bummerl) {
        this.bummerl = bummerl;
    }

    public int getSchneider() {
        return schneider;
    }

    public void setSchneider(int schneider) {
        this.schneider = schneider;
    }

    public int getRetourSchneider() {
        return retourSchneider;
    }

    public void setRetourSchneider(int retourSchneider) {
        this.retourSchneider = retourSchneider;
    }

    @Override
    public String toString() {
        if (players == null || players.isEmpty()) {
            return "";
        }

        StringBuilder teamName = new StringBuilder();
        for (Player player : players) {
            teamName.append(player.getName()).append(TEAM_NAME_SEPARATOR);
        }

        teamName.setLength(teamName.length() - TEAM_NAME_SEPARATOR.length());
        return teamName.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Team team = (Team) o;

        if (id != team.id) return false;
        if (points != team.points) return false;
        if (bummerl != team.bummerl) return false;
        if (schneider != team.schneider) return false;
        if (retourSchneider != team.retourSchneider) return false;
        return players != null ? players.equals(team.players) : team.players == null;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (players != null ? players.hashCode() : 0);
        result = 31 * result + points;
        result = 31 * result + bummerl;
        result = 31 * result + schneider;
        result = 31 * result + retourSchneider;
        return result;
    }
}
