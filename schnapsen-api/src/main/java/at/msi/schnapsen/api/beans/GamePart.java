package at.msi.schnapsen.api.beans;

import java.io.Serializable;
import java.util.Date;

/**
 * Representation of a partial game
 * Created by Michael Sieber on 22.02.2017.
 */
public final class GamePart implements Serializable {
    private Date start;
    private Date end;

    public Date getStart() {
        return start;
    }

    public void setStart(Date start) {
        this.start = start;
    }

    public Date getEnd() {
        return end;
    }

    public void setEnd(Date end) {
        this.end = end;
    }

    @Override
    public String toString() {
        return "GamePart{" +
                "start=" + start +
                ", end=" + end +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        GamePart gamePart = (GamePart) o;

        if (start != null ? !start.equals(gamePart.start) : gamePart.start != null) return false;
        return end != null ? end.equals(gamePart.end) : gamePart.end == null;
    }

    @Override
    public int hashCode() {
        int result = start != null ? start.hashCode() : 0;
        result = 31 * result + (end != null ? end.hashCode() : 0);
        return result;
    }
}
