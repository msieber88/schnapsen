package at.msi.schnapsen.api.beans;

import org.junit.Test;
import org.meanbean.lang.Factory;
import org.meanbean.test.Configuration;
import org.meanbean.test.ConfigurationBuilder;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;


/**
 * Test for {@link Team}
 * Created by Michael Sieber on 18.02.2017.
 */
public class TeamTest extends AbstractBeanTest<Team> {

    @Override
    protected Team getBeanInstance() {
        return new Team();
    }

    @Override
    protected Configuration getConfiguration() {
        return new ConfigurationBuilder().overrideFactory("players", new PlayersFactory()).build();
    }

    /**
     * Test team toString with empty players
     */
    @Test
    public void toStringEmptyPlayers(){
        Team team = new Team();
        team.setPlayers(new ArrayList<Player>());
        assertEquals("", team.toString());
    }

    /**
     * Test team toString with null players
     */
    @Test
    public void toStringNullPlayers(){
        Team team = new Team();
        team.setPlayers(null);
        assertEquals("", team.toString());
    }

    /**
     * Test team toString with a single player
     */
    @Test
    public void toStringSinglePlayer(){
        Player player = new Player();
        player.setName("TestPlayer");
        List<Player> players = new ArrayList<Player>();
        players.add(player);
        Team team = new Team();
        team.setPlayers(players);
        assertEquals("TestPlayer", team.toString());
    }

    /**
     * Test team toString with two players
     */
    @Test
    public void toStringTwoPlayers(){
        Player player1 = new Player();
        player1.setName("Player 1");
        Player player2 = new Player();
        player2.setName("Player 2");
        List<Player> players = new ArrayList<Player>();
        players.add(player1);
        players.add(player2);
        Team team = new Team();
        team.setPlayers(players);
        assertEquals("Player 1 & Player 2", team.toString());
    }

    class PlayersFactory implements Factory<List<Player>> {

        public List<Player> create() {
            List<Player> players = new ArrayList<Player>();
            Player p = new Player();
            p.setName("foo");
            players.add(p);
            return players;
        }
    }

}
