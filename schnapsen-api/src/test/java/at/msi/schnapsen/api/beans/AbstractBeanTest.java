package at.msi.schnapsen.api.beans;

import static org.junit.Assert.assertEquals;

import java.io.Serializable;

import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;
import org.apache.commons.lang3.SerializationUtils;
import org.junit.Test;
import org.meanbean.test.BeanTester;
import org.meanbean.test.Configuration;

/**
 * Base class for bean testing with common test methods
 * Created by Michael Sieber on 18.02.2017.
 *
 * @param <T> The bean class to test
 */
public abstract class AbstractBeanTest<T> {
    protected String[] propertiesToBeIgnored;

    /**
     * Test if the bean is serializable
     *
     * @throws Exception Thrown on serialization errors
     */
    @Test
    public void beanIsSerializable() throws Exception {
        final T myBean = getBeanInstance();
        final byte[] serializedMyBean = SerializationUtils.serialize((Serializable) myBean);
        @SuppressWarnings("unchecked")
        final T deserializedMyBean = (T) SerializationUtils.deserialize(serializedMyBean);
        assertEquals(myBean, deserializedMyBean);
    }

    /**
     * Test equals and hashCode function
     */
    @Test
    public void equalsAndHashCodeContract() {
        EqualsVerifier.forClass(getBeanInstance().getClass()).suppress(Warning.STRICT_INHERITANCE, Warning.NONFINAL_FIELDS).verify();
    }

    /**
     * Test getters and setters
     *
     * @throws Exception Thrown on errors
     */
    @Test
    public void getterAndSetterCorrectness() throws Exception {
        final BeanTester beanTester = new BeanTester();
        beanTester.testBean(getBeanInstance().getClass(), getConfiguration());
    }

    /**
     * Get an instance of the bean which should be tested
     *
     * @return The initialized bean object to test
     */
    protected abstract T getBeanInstance();

    /**
     * Overwrite the optional configuration for getter and setter testing
     *
     * @return The getter and setter configuration or null if no special configuration is needed
     */
    protected Configuration getConfiguration() {
        return null;
    }
}
