package at.msi.schnapsen.api.beans;

/**
 * Tests for {@link Game}
 * Created by Michael on 22.02.2017.
 */
public class GameTest extends AbstractBeanTest<Game> {
    protected Game getBeanInstance() {
        return new Game();
    }
}
