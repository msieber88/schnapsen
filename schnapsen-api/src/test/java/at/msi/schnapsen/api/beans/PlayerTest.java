package at.msi.schnapsen.api.beans;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;


/**
 * Test for {@link Player}
 * Created by Michael Sieber on 18.02.2017.
 */
public class PlayerTest extends AbstractBeanTest<Player> {

    @Override
    protected Player getBeanInstance() {
        return new Player();
    }

    /**
     * Test correct implementation of toString which should return the name of the player
     */
    @Test
    public void testToString() {
        Player p = new Player();
        p.setName("Test");
        assertEquals("Test", p.toString());
    }

    /**
     * Test null name in toString
     */
    @Test
    public void testToStringNull() {
        Player p = new Player();
        assertNull(p.toString());
    }
}
