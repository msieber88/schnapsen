package at.msi.schnapsen.api.beans;

/**
 * Tests for {@link GamePart}
 * Created by Michael on 22.02.2017.
 */
public class GamePartTest extends AbstractBeanTest<GamePart> {
    protected GamePart getBeanInstance() {
        return new GamePart();
    }
}
